import numpy as np
import pandas as pd

import tensorflow as tf
from tensorflow import keras

from sklearn.manifold import TSNE

import argparse
import os

from time import time
import matplotlib.pyplot as plt
import aeutils


tensorboard = keras.callbacks.TensorBoard(log_dir="logs/{}".format(time()))
modelcheckpoint = keras.callbacks.ModelCheckpoint('./checkpoints/weights.{epoch:02d}-{val_loss:.2f}', monitor='val_loss',
                                                  verbose=0, save_best_only=False, save_weights_only=True,
                                                  mode='auto', period=100)
reduceLR = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5,
                                             verbose=1, mode='auto', min_delta=0.0001, cooldown=10, min_lr=0.00005)

epochs = 150
batch_size = 256


# reparameterization trick
# instead of sampling from Q(z|X), sample eps = N(0,I)
# z = z_mean + sqrt(var)*eps
def sampling(args):
    """Reparameterization trick by sampling fr an isotropic unit Gaussian.
    # Arguments:
        args (tensor): mean and log of variance of Q(z|X)
    # Returns:
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args
    batch = keras.backend.shape(z_mean)[0]
    dim = keras.backend.int_shape(z_mean)[1]
    # by default, random_normal has mean=0 and std=1.0
    epsilon = keras.backend.random_normal(shape=(batch, dim))
    result = z_mean + keras.backend.exp(0.5 * z_log_var) * epsilon
    return result

def define_model(input_size, latent_dim=32, dropout_rate=0.25, activation='sigmoid', intermediate_dim=640, hidden_layers_num=2):

    # network parameters
    input_shape = (input_size, )

    # VAE model = encoder + decoder
    # build encoder model
    inputs = keras.Input(shape=input_shape, name='encoder_input')
    x = inputs
    for i in range(hidden_layers_num):
        y = keras.layers.Dense(intermediate_dim, activation=activation)(x)
        dropout = keras.layers.Dropout(dropout_rate)(y)
        x = dropout
    z_mean = keras.layers.Dense(
        latent_dim, name='z_mean')(x)
    z_log_var = keras.layers.Dense(
        latent_dim, activation='sigmoid', name='z_log_var')(x)
    z = keras.layers.Lambda(sampling, output_shape=(
        latent_dim,), name='z')([z_mean, z_log_var])

    # instantiate encoder model
    encoder = keras.models.Model(
        inputs, [z_mean, z_log_var, z], name='encoder')
    encoder.summary()

    # build decoder model
    latent_inputs = keras.Input(shape=(latent_dim,), name='z_sampling')
    x = latent_inputs
    for i in range(hidden_layers_num):
        y = keras.layers.Dense(intermediate_dim, activation=activation)(x)
        dropout = keras.layers.Dropout(dropout_rate)(y)
        x = dropout
    outputs = keras.layers.Dense(input_size, activation='sigmoid')(x)

    # instantiate decoder model
    decoder = keras.models.Model(latent_inputs, outputs, name='decoder')
    decoder.summary()

    # instantiate VAE model
    outputs = decoder(encoder(inputs)[2])
    vaerec = keras.models.Model(inputs, outputs, name='vaerec')

    return vaerec, input_size, z_mean, z_log_var, encoder


def loss_wrapper(input_size, z_mean, z_log_var, beta=1, rec_loss=tf.losses.log_loss):
    def vae_loss(y_true, y_pred):
        reconstruction_loss = rec_loss(y_true, y_pred)
        reconstruction_loss *= input_size
        kl_loss = 1 + z_log_var - \
            keras.backend.square(z_mean) - keras.backend.exp(z_log_var)
        kl_loss = keras.backend.sum(kl_loss, axis=-1)
        kl_loss *= -0.5
        result = keras.backend.mean(reconstruction_loss + beta*kl_loss)
        return result
    return vae_loss


def train_and_evaluate(hyperparams):
    keras.backend.clear_session()

    vaerec, input_size, z_mean, z_log_var, encoder = define_model(
        train.shape[1], latent_dim=16)

    optimizer = keras.optimizers.Adam(lr=0.001)
    vaerec.compile(optimizer=optimizer, loss=loss_wrapper(
        input_size, z_mean, z_log_var))
    vaerec.summary()

    # train the autoencoder
    vaerec.fit(train,
               train,
               epochs=epochs,
               batch_size=batch_size,
               validation_data=(val, val),
               callbacks=[tensorboard, modelcheckpoint, reduceLR])
    vaerec.save_weights('./checkpoints/vaerec_ml20')

    predicted_ratings = vaerec.predict(
        prediction_batch,
        batch_size=batch_size)

    aeutils.calculate_metrics(
        predicted_ratings, heldout_batch, hyperparams)
        
    predictions = encoder.predict(
        prediction_batch,
        batch_size=batch_size)[2]

    print(predictions)

    return predictions


parser = argparse.ArgumentParser()
parser.add_argument('-d', '--data', dest='input_file', action='store', required=True,
                        help='CSV file with input data')

args = parser.parse_args()

df = pd.read_csv(args.input_file,sep=',',nrows=15000000, usecols=['userId','movieId','rating'], dtype={'userId':np.int32,'movieId':np.int32,'rating':np.float32})

df = df[df['rating']>4]

data = df.pivot(index='userId', columns='movieId', values='rating')

data = data.fillna(0)

data[data != 0] = 1

loaded_file = data.values
file_chunk_size = loaded_file.shape[0]//5
global train, val, prediction_batch, heldout_batch
train = loaded_file[0:file_chunk_size*3]
val = loaded_file[file_chunk_size*3:file_chunk_size*4]
test = loaded_file[file_chunk_size*4:-1]

heldout_items_size = 0.2

mask_matrix = np.zeros(test.shape, dtype=bool)
mask_rows = np.zeros(test.shape[0], dtype=bool)

for i in range(0, test.shape[0]):
    row = test[i,:]
    indices = np.flatnonzero(row)
    heldout_indices = np.random.choice(
    indices, int(heldout_items_size*len(indices)), replace=False)
    if len(heldout_indices) == 0:
        mask_rows[i] = True  # Eliminate empty rows
    mask_matrix[i, heldout_indices] = True

matrix_dense = test.astype(bool)
heldout_batch = np.logical_and(mask_matrix, matrix_dense)[~mask_rows]
prediction_batch = np.logical_and(~mask_matrix, matrix_dense)[~mask_rows]

predictions = train_and_evaluate([])

latent_space = pd.DataFrame(predictions, index=data.index[file_chunk_size*4:-1][~mask_rows])

tsne = TSNE(n_components=2, random_state=42, metric='cosine')
X_2d = tsne.fit_transform(latent_space)
X_2d_df = pd.DataFrame(X_2d, columns=["x_1", "x_2"],index=latent_space.index)
latent_space = pd.concat([latent_space, X_2d_df], axis=1)

latent_space.to_csv("logs/latent.csv")
