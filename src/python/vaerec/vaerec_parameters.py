import tensorflow as tf
from tensorflow import keras

from time import time
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
from scipy import sparse

from skopt import gp_minimize, dummy_minimize
from skopt.space import Real, Integer, Categorical
from skopt import dump

import csv
import aeutils


tensorboard = keras.callbacks.TensorBoard(log_dir="logs/{}".format(time()))
modelcheckpoint = keras.callbacks.ModelCheckpoint('./checkpoints/weights.{epoch:02d}-{val_loss:.2f}', monitor='val_loss',
                                                  verbose=0, save_best_only=False, save_weights_only=True,
                                                  mode='auto', period=100)
# reduceLR = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5,
#                                             verbose=1, mode='auto', min_delta=0.0001, cooldown=10, min_lr=0.00005)

epochs = 85
batch_size = 256


# reparameterization trick
# instead of sampling from Q(z|X), sample eps = N(0,I)
# z = z_mean + sqrt(var)*eps
def sampling(args):
    """Reparameterization trick by sampling fr an isotropic unit Gaussian.
    # Arguments:
        args (tensor): mean and log of variance of Q(z|X)
    # Returns:
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args
    batch = keras.backend.shape(z_mean)[0]
    dim = keras.backend.int_shape(z_mean)[1]
    # by default, random_normal has mean=0 and std=1.0
    epsilon = keras.backend.random_normal(shape=(batch, dim))
    result = z_mean + keras.backend.exp(0.5 * z_log_var) * epsilon
    return result


def define_model(input_size, latent_dim=32, dropout_rate=0.25, activation='sigmoid', intermediate_dim=640, hidden_layers_num=1):

    # network parameters
    input_shape = (input_size, )

    # VAE model = encoder + decoder
    # build encoder model
    inputs = keras.Input(shape=input_shape, name='encoder_input')
    x = inputs
    for i in range(hidden_layers_num):
        y = keras.layers.Dense(intermediate_dim, activation=activation)(x)
        dropout = keras.layers.Dropout(dropout_rate)(y)
        x = dropout
    z_mean = keras.layers.Dense(
        latent_dim, name='z_mean')(x)
    z_log_var = keras.layers.Dense(
        latent_dim, activation='sigmoid', name='z_log_var')(x)
    z = keras.layers.Lambda(sampling, output_shape=(
        latent_dim,), name='z')([z_mean, z_log_var])

    # instantiate encoder model
    encoder = keras.models.Model(
        inputs, [z_mean, z_log_var, z], name='encoder')
    encoder.summary()

    # build decoder model
    latent_inputs = keras.Input(shape=(latent_dim,), name='z_sampling')
    x = latent_inputs
    for i in range(hidden_layers_num):
        y = keras.layers.Dense(intermediate_dim, activation=activation)(x)
        dropout = keras.layers.Dropout(dropout_rate)(y)
        x = dropout
    outputs = keras.layers.Dense(input_size, activation='sigmoid')(x)

    # instantiate decoder model
    decoder = keras.models.Model(latent_inputs, outputs, name='decoder')
    decoder.summary()

    # instantiate VAE model
    outputs = decoder(encoder(inputs)[2])
    vaerec = keras.models.Model(inputs, outputs, name='vaerec')

    return vaerec, input_size, z_mean, z_log_var


def loss_wrapper(input_size, z_mean, z_log_var, beta=0.25, rec_loss=tf.losses.log_loss):
    def vae_loss(y_true, y_pred):
        reconstruction_loss = rec_loss(y_true, y_pred)
        reconstruction_loss *= input_size
        kl_loss = 1 + z_log_var - \
            keras.backend.square(z_mean) - keras.backend.exp(z_log_var)
        kl_loss = keras.backend.sum(kl_loss, axis=-1)
        kl_loss *= -0.5
        result = keras.backend.mean(reconstruction_loss + beta*kl_loss)
        return result
    return vae_loss


def train_and_evaluate(hyperparams):
    print(hyperparams)
    keras.backend.clear_session()

    vaerec, input_size, z_mean, z_log_var = define_model(
        train.shape[1])

    optimizer = keras.optimizers.Adam(lr=0.0015)
    vaerec.compile(optimizer=optimizer, loss=loss_wrapper(
        input_size, z_mean, z_log_var, beta=hyperparams[0]))
    vaerec.summary()

    # train the autoencoder
    vaerec.fit(train,
               train,
               epochs=epochs,
               batch_size=batch_size,
               validation_data=(val, val),
               callbacks=[tensorboard, modelcheckpoint])
    vaerec.save_weights('./checkpoints/vaerec_ml20')

    predicted_ratings = vaerec.predict(
        prediction_batch,
        batch_size=batch_size)

    ndcg100 = aeutils.calculate_metrics(
        predicted_ratings, heldout_batch, hyperparams)

    return -ndcg100


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data', dest='input_file', action='store', required=True,
                        help='CSV file with input data')

    args = parser.parse_args()
    loaded_file = sparse.load_npz(args.input_file)
    file_chunk_size = loaded_file.shape[0]//5
    global train, val, prediction_batch, heldout_batch
    train = loaded_file[0:file_chunk_size*3]
    val = loaded_file[file_chunk_size*3:file_chunk_size*4]
    test = loaded_file[file_chunk_size*4:-1]

    heldout_items_size = 0.2
    heldout_batch, prediction_batch = aeutils.create_test_set(
        test, heldout_items_size)

    hyperparam_config = [Real(0, 1, name="beta")]

    aeutils.save_results(["beta",
                          "Recall@20", "Recall@50", "NDCG@100", "Coverage"])

    res = dummy_minimize(train_and_evaluate, hyperparam_config,
                         n_calls=15, verbose=True)
    print(res)
    dump(res, "logs/optimize.gz", store_objective=False)
