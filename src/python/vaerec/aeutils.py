import tensorflow as tf
from tensorflow import keras

from time import time
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
from scipy import sparse

from skopt import gp_minimize
from skopt.space import Real, Integer, Categorical
from skopt import dump

import csv


def masked_mse(y_true, y_pred):
    zero = keras.backend.constant(0, dtype=keras.backend.floatx())
    mask_true = keras.backend.cast(keras.backend.not_equal(
        y_true, zero), keras.backend.floatx())
    masked_squared_error = keras.backend.square(mask_true * (y_true - y_pred))
    masked_mse = keras.backend.sum(
        masked_squared_error, axis=-1) / keras.backend.sum(mask_true, axis=-1)
    return masked_mse


def create_test_set(matrix, heldout_items_size):
    mask_matrix = np.zeros(matrix.shape, dtype=bool)
    mask_rows = np.zeros(matrix.shape[0], dtype=bool)

    for i in range(0, matrix.shape[0]):
        row = matrix.getrow(i)
        indices = row.indices
        heldout_indices = np.random.choice(
            indices, int(heldout_items_size*len(indices)), replace=False)
        if len(heldout_indices) == 0:
            mask_rows[i] = True  # Eliminate empty rows
        mask_matrix[i, heldout_indices] = True

    matrix_dense = matrix.astype(bool).todense()
    heldout_batch = np.logical_and(mask_matrix, matrix_dense)[~mask_rows]
    prediction_batch = np.logical_and(~mask_matrix, matrix_dense)[~mask_rows]

    print('Matrix shape:')
    print(heldout_batch.shape)
    print(prediction_batch.shape)

    return heldout_batch, prediction_batch


'''
Metrics are based on
 https://github.com/dawenl/vae_cf
'''


def Recall_at_k_batch(X_pred, heldout_batch, k=50):
    batch_users = X_pred.shape[0]

    idx = np.argpartition(-X_pred, k, axis=1)
    X_pred_binary = np.zeros_like(X_pred, dtype=bool)
    X_pred_binary[np.arange(batch_users)[:, np.newaxis], idx[:, :k]] = True

    X_true_binary = (heldout_batch > 0)
    tmp = (np.logical_and(X_true_binary, X_pred_binary).sum(axis=1)).astype(
        np.float32)
    recall = tmp / np.minimum(k, X_true_binary.sum(axis=1))
    return recall


def NDCG_binary_at_k_batch(X_pred, heldout_batch, k=50):
    '''
    normalized discounted cumulative gain@k for binary relevance
    ASSUMPTIONS: all the 0's in heldout_data indicate 0 relevance
    '''
    batch_users = X_pred.shape[0]
    idx_topk_part = np.argpartition(-X_pred, k, axis=1)
    topk_part = X_pred[np.arange(batch_users)[:, np.newaxis],
                       idx_topk_part[:, :k]]
    idx_part = np.argsort(-topk_part, axis=1)
    # X_pred[np.arange(batch_users)[:, np.newaxis], idx_topk] is the sorted top-k predicted score
    idx_topk = idx_topk_part[np.arange(batch_users)[:, np.newaxis], idx_part]
    # build the discount template
    tp = 1. / np.log2(np.arange(2, k + 2))

    DCG = (np.array(heldout_batch[np.arange(batch_users)[:, np.newaxis],
                                  idx_topk]) * tp).sum(axis=1)
    IDCG = np.array([(tp[:min(n, k)]).sum()
                     for n in np.ravel(np.count_nonzero(heldout_batch, axis=1))])
    return DCG / IDCG


def coverage(X_pred, items_count, k=50):
    batch_users = X_pred.shape[0]
    idx_topk_part = np.argpartition(-X_pred, k, axis=1)
    topk_part = X_pred[np.arange(batch_users)[:, np.newaxis],
                       idx_topk_part[:, :k]]
    idx_part = np.argsort(-topk_part, axis=1)
    idx_topk = idx_topk_part[np.arange(batch_users)[:, np.newaxis], idx_part]

    return np.unique(idx_topk).shape[0]/items_count


def save_results(results):
    with open("logs/results.csv", "a") as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow(results)


def calculate_metrics(predicted_ratings, heldout_batch, hyperparams):
    coverage50 = coverage(predicted_ratings, heldout_batch.shape[1], 50)
    recall20 = np.mean(Recall_at_k_batch(predicted_ratings, heldout_batch, 20))
    recall50 = np.mean(Recall_at_k_batch(predicted_ratings, heldout_batch, 50))
    ndcg100 = np.mean(NDCG_binary_at_k_batch(
        predicted_ratings, heldout_batch, 100))

    print('Recall@{}: {}'.format(20, recall20))
    print('Recall@{}: {}'.format(50, recall50))
    print('NDCG@{}: {}'.format(100, ndcg100))
    print('Coverage@{}: {}'.format(50, coverage50))

    save_results(hyperparams + [recall20, recall50, ndcg100, coverage50])
    return ndcg100
