import pandas as pd
import numpy as np
from scipy import sparse

df = pd.read_csv('netflix_ratings.csv', sep=',', nrows=20000000, usecols=['userId','movieId','rating'], dtype={'userId':np.int32,'movieId':np.int32,'rating':np.float32})

data = df.pivot(index='userId', columns='movieId', values='rating')

data = data.fillna(0)

data[data != 0] = 1

save = sparse.csr_matrix(data.values, dtype=np.float32)

sparse.save_npz('netflix_user_movie_rating_10m.npz', save)