import tensorflow as tf
from tensorflow import keras

from time import time
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os
from scipy import sparse

from skopt import gp_minimize, dummy_minimize
from skopt.space import Real, Integer, Categorical
from skopt import dump

import csv
import aeutils


tensorboard = keras.callbacks.TensorBoard(log_dir="logs/{}".format(time()))
modelcheckpoint = keras.callbacks.ModelCheckpoint('./checkpoints/weights.{epoch:02d}-{val_loss:.2f}', monitor='val_loss',
                                                  verbose=0, save_best_only=False, save_weights_only=True,
                                                  mode='auto', period=100)
# reduceLR = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5,
#                                             verbose=1, mode='auto', min_delta=0.0001, cooldown=10, min_lr=0.00005)

epochs = 60
batch_size = 256


def define_model(input_size, latent_dim=32, dropout_rate=0.75, activation='relu', intermediate_dim=512, l1_rate=10e-9, l2_rate=10e-6, hidden_layers_num=1):

    # network parameters
    input_shape = (input_size, )

    # Autoencoder model = encoder + decoder
    # build encoder model
    inputs = keras.Input(shape=input_shape, name='encoder_input')
    x = inputs
    for i in range(hidden_layers_num):
        y = keras.layers.Dense(intermediate_dim, activation=activation)(x)
        dropout = keras.layers.Dropout(dropout_rate)(y)
        x = dropout
    latent = keras.layers.Dense(
        latent_dim, name='latent', activity_regularizer=keras.regularizers.l1(l1_rate))(x)

    # instantiate encoder model
    encoder = keras.models.Model(inputs, latent, name='encoder')
    encoder.summary()

    # build decoder model
    latent_inputs = keras.Input(shape=(latent_dim,), name='decoder_input')
    x = latent_inputs
    for i in range(hidden_layers_num):
        y = keras.layers.Dense(intermediate_dim, activation=activation)(x)
        dropout = keras.layers.Dropout(dropout_rate)(y)
        x = dropout
    outputs = keras.layers.Dense(input_size, activation='sigmoid')(x)

    # instantiate decoder model
    decoder = keras.models.Model(latent_inputs, outputs, name='decoder')
    decoder.summary()

    # instantiate autoencoder model
    outputs = decoder(encoder(inputs))
    autorec = keras.models.Model(inputs, outputs, name='autorec')

    return autorec


def train_and_evaluate(hyperparams):
    print(hyperparams)
    keras.backend.clear_session()
    autorec = define_model(train.shape[1], l1_rate=hyperparams[0])

    optimizer = keras.optimizers.Adam(lr=0.001)
    autorec.compile(optimizer=optimizer, loss=tf.losses.log_loss)
    autorec.summary()

    # train the autoencoder
    autorec.fit(train,
                train,
                epochs=epochs,
                batch_size=batch_size,
                validation_data=(val, val),
                callbacks=[tensorboard, modelcheckpoint])
    autorec.save_weights('./checkpoints/autorec_ml20')

    predicted_ratings = autorec.predict(
        prediction_batch,
        batch_size=batch_size)

    ndcg100 = aeutils.calculate_metrics(
        predicted_ratings, heldout_batch, hyperparams)

    return -ndcg100


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data', dest='input_file', action='store', required=True,
                        help='CSV file with input data')

    args = parser.parse_args()
    loaded_file = sparse.load_npz(args.input_file)
    file_chunk_size = loaded_file.shape[0]//5
    global train, val, prediction_batch, heldout_batch
    train = loaded_file[0:file_chunk_size*3]
    val = loaded_file[file_chunk_size*3:file_chunk_size*4]
    test = loaded_file[file_chunk_size*4:-1]

    heldout_items_size = 0.2
    heldout_batch, prediction_batch = aeutils.create_test_set(
        test, heldout_items_size)

    hyperparam_config = [Real(0, 0.001, name="l1_rate")]

    aeutils.save_results(["l1_rate",
                          "Recall@20", "Recall@50", "NDCG@100", "Coverage"])

    res = gp_minimize(train_and_evaluate, hyperparam_config,
                      n_calls=35, verbose=True)
    print(res)
    dump(res, "logs/optimize.gz", store_objective=False)
