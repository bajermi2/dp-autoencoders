import tensorflow as tf
from tensorflow import keras

from time import time
import numpy as np
import argparse
import os
from scipy import sparse

'''
Based on
 http://sujitpal.blogspot.com/2018/05/singular-value-decomposition-for.html
'''


def define_model(nb_users, nb_items, nb_factors):
    # User-item matrix
    user_item_matrix = tf.placeholder(
        tf.float32, shape=(nb_users, nb_items))

    # SVD
    St, Ut, Vt = tf.svd(user_item_matrix, full_matrices=True)

    # Compute reduced matrices
    Sk = tf.diag(St)[0:nb_factors, 0:nb_factors]
    Uk = Ut[:, 0:nb_factors]
    Vk = tf.transpose(Vt)[0:nb_factors, :]

    # Compute user ratings_model
    ratings = tf.matmul(Uk, tf.matmul(Sk, Vk))
    rmse = tf.metrics.root_mean_squared_error(user_item_matrix, ratings)[1]

    return ratings, user_item_matrix, rmse


def create_test_set(matrix, heldout_items_size):
    mask_matrix = np.zeros(matrix.shape, dtype=bool)
    mask_rows = np.zeros(matrix.shape[0], dtype=bool)

    for i in range(0, matrix.shape[0]):
        row = matrix.getrow(i)
        indices = row.indices
        heldout_indices = np.random.choice(
            indices, int(heldout_items_size*len(indices)), replace=False)
        if len(heldout_indices) == 0:
            mask_rows[i] = True  # Eliminate empty rows
        mask_matrix[i, heldout_indices] = True

    matrix = matrix.astype(bool).todense()
    heldout_batch = np.logical_and(mask_matrix, matrix)[~mask_rows]
    prediction_batch = np.logical_and(~mask_matrix, matrix)[~mask_rows]

    print('Matrix shape:')
    print(heldout_batch.shape)
    print(prediction_batch.shape)

    return heldout_batch, prediction_batch


'''
Metrics are based on
 https://github.com/dawenl/vae_cf
'''


def Recall_at_k_batch(X_pred, heldout_batch, k=50):
    batch_users = X_pred.shape[0]

    idx = np.argpartition(-X_pred, k, axis=1)
    X_pred_binary = np.zeros_like(X_pred, dtype=bool)
    X_pred_binary[np.arange(batch_users)[:, np.newaxis], idx[:, :k]] = True

    X_true_binary = (heldout_batch > 0)
    tmp = (np.logical_and(X_true_binary, X_pred_binary).sum(axis=1)).astype(
        np.float32)
    recall = tmp / np.minimum(k, X_true_binary.sum(axis=1))
    return recall


def NDCG_binary_at_k_batch(X_pred, heldout_batch, k=50):
    '''
    normalized discounted cumulative gain@k for binary relevance
    ASSUMPTIONS: all the 0's in heldout_data indicate 0 relevance
    '''
    batch_users = X_pred.shape[0]
    idx_topk_part = np.argpartition(-X_pred, k, axis=1)
    topk_part = X_pred[np.arange(batch_users)[:, np.newaxis],
                       idx_topk_part[:, :k]]
    idx_part = np.argsort(-topk_part, axis=1)
    # X_pred[np.arange(batch_users)[:, np.newaxis], idx_topk] is the sorted top-k predicted score
    idx_topk = idx_topk_part[np.arange(batch_users)[:, np.newaxis], idx_part]
    # build the discount template
    tp = 1. / np.log2(np.arange(2, k + 2))

    DCG = (np.array(heldout_batch[np.arange(batch_users)[:, np.newaxis],
                                  idx_topk]) * tp).sum(axis=1)
    IDCG = np.array([(tp[:min(n, k)]).sum()
                     for n in np.ravel(np.count_nonzero(heldout_batch, axis=1))])
    return DCG / IDCG


def coverage(X_pred, items_count, k=50):
    batch_users = X_pred.shape[0]
    idx_topk_part = np.argpartition(-X_pred, k, axis=1)
    topk_part = X_pred[np.arange(batch_users)[:, np.newaxis],
                       idx_topk_part[:, :k]]
    idx_part = np.argsort(-topk_part, axis=1)
    idx_topk = idx_topk_part[np.arange(batch_users)[:, np.newaxis], idx_part]

    return np.unique(idx_topk).shape[0]/items_count


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data', dest='input_file', action='store', required=True,
                        help='CSV file with input data')

    args = parser.parse_args()
    matrix = sparse.load_npz(args.input_file)

    heldout_items_size = 0.2

    heldout_batch, prediction_batch = create_test_set(
        matrix, heldout_items_size)

    graph = tf.Graph()
    with graph.as_default():
        ratings_model, user_item_matrix, rmse = define_model(
            prediction_batch.shape[0], prediction_batch.shape[1], 50)

    session = tf.InteractiveSession(graph=graph)

    tf.global_variables_initializer().run()
    tf.local_variables_initializer().run()

    feed_dict = {
        user_item_matrix: prediction_batch
    }

    predicted_ratings, rmse_val = session.run(
        [ratings_model, rmse], feed_dict=feed_dict)

    coverage50 = coverage(predicted_ratings, matrix.shape[1], 50)
    recall20 = Recall_at_k_batch(predicted_ratings, heldout_batch, 20)
    recall50 = Recall_at_k_batch(predicted_ratings, heldout_batch, 50)
    ndcg100 = NDCG_binary_at_k_batch(predicted_ratings, heldout_batch, 100)

    print('Recall@{}: {}'.format(20, np.mean(recall20)))
    print('Recall@{}: {}'.format(50, np.mean(recall50)))
    print('NDCG@{}: {}'.format(100, np.mean(ndcg100)))
    print('Coverage@{}: {}'.format(50, coverage50))
    print('RMSE: {}'.format(rmse_val))
