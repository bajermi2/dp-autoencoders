#!/bin/bash
# sets home directory
DATADIR="/storage/brno2/home/bajermi2"
DATAFILEDIR="/storage/brno2/home/bajermi2"
# loads the tensorflow module
module add cuda-9.0
module add cudnn-7.0
module add python-3.6.2-gcc
module load debian8-compat

export PYTHONUSERBASE=/storage/praha1/home/bajermi2/.local
export PATH=$PYTHONUSERBASE/bin:$PATH
export PYTHONPATH=$PYTHONUSERBASE/lib/python3.6/site-packages:$PYTHONPATH

FOLDER=ml-20m
DATAFILENAME=user_movie_rating_20m.npz
#DATAFILENAME=ratings.csv

#FOLDER=netflix
#DATAFILENAME=netflix_user_movie_rating_20m.npz

# setup SCRATCH cleaning in case of an error
trap 'clean_scratch' TERM EXIT
# enters user's scratch directory
cd $SCRATCHDIR || exit 1
# prepare data
cp $DATADIR/AutoRec/* $SCRATCHDIR
cp $DATADIR/aeutils/* $SCRATCHDIR
cp $DATAFILEDIR/$FOLDER/$DATAFILENAME $SCRATCHDIR
# starts the application
mkdir checkpoints
mkdir logs
echo "DATAFILE: ${FOLDER}/${DATAFILENAME}"
python3 autorec_1HL.py --d $DATAFILENAME
# moves the produced (output) data to user's home directory or leave it in SCRATCH if error occured
savetime=$(date +%s)
cp -r logs/ $DATADIR/AutoRec/logs-$savetime/ || export CLEAN_SCRATCH=false
cp -r checkpoints/ $DATADIR/AutoRec/checkpoints-$savetime/ || export CLEAN_SCRATCH=false
